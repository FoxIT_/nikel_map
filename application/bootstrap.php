<?php
	
	// при загрузке получаем язык пользователя для определения языковой версии сайта
	require_once 'core/language.php';
	
	// подключаем файлы каркаса	
	require_once 'models/model.php';
	require_once 'views/view.php';
	require_once 'controllers/controller.php';
	
require_once 'core/route.php'; // подключаем маршрутизатор
Route::start(); // запускаем маршрутизатор

?>