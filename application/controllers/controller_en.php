<?
class Controller_en extends Controller
{
	public $model;
	public $view;
	
	function __construct()
	{
		$this->view = new View();

	}
	
	function action_index()
	{
		$_SESSION['lang'] = 'en';
		$this->view->generate('main_view.php', 'template_view.php');
	}
	
	
}
?>