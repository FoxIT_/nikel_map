<?php

class Controller_Properties extends Controller
{

	function __construct()
	{
		$this->view = new View();
	}
	
	function action_index()
	{
		$this->view->generate('properties_view.php', 'template_view.php');
	}
}

?>