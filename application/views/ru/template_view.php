
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<title>Nikel Materiality</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/leaflet.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<script type="text/javascript" src="js/jquery-1.6.2.js" ></script>
		<script type="text/javascript" src="js/leaflet.js"></script>
		<script type="text/javascript" src="js/leafletembed.js"></script>

	</head>
	
<body onload="initMap()">
	<header>
	
		<!-- Logo -->
		<a href="/Main"><div class="logo">
			Никель<br> Материальность
		</div></a>
		<!-- Main menu -->
		<ul class="main-menu">
			<li><a href="/Main">Главная</a></li>
			<li><a href="/About">О проекте</a></li>
			<li><a href="/News">Новости</a></li>
			<li><a href="/Properties">Свойства</a></li>
			<li><a href="/Map">Карта</a></li>
			<li><a href="/Contacts">Контакты</a></li>
		</ul>
		<!-- Language switch-->
		<div class="lang-icon">
			<a href="/ru"><div class="rus select"></div></a>
			<a href="/en"><div class="eng"></div></a>
		</div>
		
	</header>
	
	<?php include 'application/views/'.$_SESSION['lang'].'/'.$content_view; ?>

	</body>

</html>