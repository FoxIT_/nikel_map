/**
 * Created by Home on 05.02.2016.
 */

var map;
var ajaxRequest;
var plotlist;
var plotlayers=[];

function initMap() {
    // set up the map
    map = new L.Map('map');

    // create the tile layer with correct attribution
    var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    //var localTiles = 'file:///tiles/{z}/{x}/{y}.png';
    var osmAttrib = 'Map data � <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl);

    map.addLayer(osm);

    // start the map on Nikel, Murmasnkaya obl.
    //map.setView([0, 0], 14); // for local tiles
    map.setView([69.4058, 30.2119], 14);

    //// Markers
    //var marker = L.marker([69.4058, 30.2119]).addTo(map);
    //marker.bindPopup('<div class="marker-1">Marker 1</div>').openPopup();

    // Circle
    var circle = L.circle([69.4058, 30.2119], 50, {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.4
    }).addTo(map);

// define rectangle geographical bounds
    var bounds = [[69.4058, 30.2119], [69.4158, 30.2219]];

// create an rectangle
    rectangle = new L.rectangle([[69.4068, 30.2219], [69.4078, 30.2319]], {color: "blue", weight: 2});
    rectangle.addTo(map);
    rectangle.bindPopup('Fuck you! I`m rectangle!');


    var popup = new L.popup();

    function onMapClick(e) {
        popup
            .setLatLng(e.latlng)
            .setContent("Click" + e.latlng.toString())
            .addTo(map);
    }

    map.on('click', onMapClick);


    var greenIcon = L.icon({
        iconUrl: 'Images/leaf-green.png',
        shadowUrl: 'Images/leaf-shadow.png',

        iconSize:     [38, 95],
        shadowSize:   [50, 64],
        iconAnchor:   [22, 94],
        shadowAnchor: [4, 62],
        popupAnchor:  [-3, -76]
    });

    L.marker([69.4098, 30.2119], {icon: greenIcon}).addTo(map);
}
